<?php

/* ::base.html.twig */
class __TwigTemplate_0f193edb20ad454fd50a31e26382534f3a3509b74d1d0c2e10d8918d78b23093 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'topbar' => array($this, 'block_topbar'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/foundation.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" />
";
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />

    </head>
    <body>

    ";
        // line 16
        $this->displayBlock('topbar', $context, $blocks);
        // line 17
        echo "
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"large-12 columns\">
            ";
        // line 21
        $this->displayBlock('body', $context, $blocks);
        // line 22
        echo "            </div>
        </div>
    </div>

        ";
        // line 26
        $this->displayBlock('javascripts', $context, $blocks);
        // line 31
        echo "
    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 16
    public function block_topbar($context, array $blocks = array())
    {
    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
    }

    // line 26
    public function block_javascripts($context, array $blocks = array())
    {
        // line 27
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/jquery.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/foundation.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 29,  99 => 28,  94 => 27,  91 => 26,  86 => 21,  81 => 16,  75 => 5,  68 => 31,  66 => 26,  60 => 22,  58 => 21,  52 => 17,  50 => 16,  41 => 11,  37 => 7,  33 => 6,  29 => 5,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         <link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}" />*/
/*         <link rel="stylesheet" href="{{ asset('css/main.css') }}" />*/
/* {#*/
/*         <link rel="stylesheet" href="{{ asset('css/main.scss') }}" />*/
/* #}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/* */
/*     </head>*/
/*     <body>*/
/* */
/*     {% block topbar %}{% endblock %}*/
/* */
/*     <div class="container">*/
/*         <div class="row">*/
/*             <div class="large-12 columns">*/
/*             {% block body %}{% endblock %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*         {% block javascripts %}*/
/*             <script src="{{ asset('js/vendor/jquery.js') }}"></script>*/
/*             <script src="{{ asset('js/vendor/foundation.min.js') }}"></script>*/
/*             <script src="{{ asset('js/app.js') }}"></script>*/
/*         {% endblock %}*/
/* */
/*     </body>*/
/* </html>*/
/* */
