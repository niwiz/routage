<?php

/* ::base.html.twig */
class __TwigTemplate_a15d6abae63018a82c83a5d9d7f618b610d55027ca4d20bf6d91c6bd5673f58a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'topbar' => array($this, 'block_topbar'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_76f180ee147d5f3a24962bc9e6abd03e46c2009a2f62d6ee8a1ceca21ebabcf6 = $this->env->getExtension("native_profiler");
        $__internal_76f180ee147d5f3a24962bc9e6abd03e46c2009a2f62d6ee8a1ceca21ebabcf6->enter($__internal_76f180ee147d5f3a24962bc9e6abd03e46c2009a2f62d6ee8a1ceca21ebabcf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/foundation.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" />
";
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body ng-app=\"MyApp\">
    ";
        // line 14
        $this->displayBlock('topbar', $context, $blocks);
        // line 15
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"large-12 columns\">
                ";
        // line 18
        $this->displayBlock('body', $context, $blocks);
        // line 19
        echo "            </div>
        </div>
    </div>
        ";
        // line 22
        $this->displayBlock('javascripts', $context, $blocks);
        // line 30
        echo "    </body>
</html>
";
        
        $__internal_76f180ee147d5f3a24962bc9e6abd03e46c2009a2f62d6ee8a1ceca21ebabcf6->leave($__internal_76f180ee147d5f3a24962bc9e6abd03e46c2009a2f62d6ee8a1ceca21ebabcf6_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_42cad0a63206e3693d5eb07fc9a44eedddc75e4993f3ffcdefe36fd6c416c053 = $this->env->getExtension("native_profiler");
        $__internal_42cad0a63206e3693d5eb07fc9a44eedddc75e4993f3ffcdefe36fd6c416c053->enter($__internal_42cad0a63206e3693d5eb07fc9a44eedddc75e4993f3ffcdefe36fd6c416c053_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_42cad0a63206e3693d5eb07fc9a44eedddc75e4993f3ffcdefe36fd6c416c053->leave($__internal_42cad0a63206e3693d5eb07fc9a44eedddc75e4993f3ffcdefe36fd6c416c053_prof);

    }

    // line 14
    public function block_topbar($context, array $blocks = array())
    {
        $__internal_f9c4ff3a27fde1f352086c61b9e807b1e706a320d553d37747c5fc1ef4fd9aae = $this->env->getExtension("native_profiler");
        $__internal_f9c4ff3a27fde1f352086c61b9e807b1e706a320d553d37747c5fc1ef4fd9aae->enter($__internal_f9c4ff3a27fde1f352086c61b9e807b1e706a320d553d37747c5fc1ef4fd9aae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "topbar"));

        
        $__internal_f9c4ff3a27fde1f352086c61b9e807b1e706a320d553d37747c5fc1ef4fd9aae->leave($__internal_f9c4ff3a27fde1f352086c61b9e807b1e706a320d553d37747c5fc1ef4fd9aae_prof);

    }

    // line 18
    public function block_body($context, array $blocks = array())
    {
        $__internal_be8e4219b2831dd40db8527be09e256c6be94b971ceef0c6f2b1b03a29bd02ab = $this->env->getExtension("native_profiler");
        $__internal_be8e4219b2831dd40db8527be09e256c6be94b971ceef0c6f2b1b03a29bd02ab->enter($__internal_be8e4219b2831dd40db8527be09e256c6be94b971ceef0c6f2b1b03a29bd02ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_be8e4219b2831dd40db8527be09e256c6be94b971ceef0c6f2b1b03a29bd02ab->leave($__internal_be8e4219b2831dd40db8527be09e256c6be94b971ceef0c6f2b1b03a29bd02ab_prof);

    }

    // line 22
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_435eedc1e619370ae35625a4e6550244d26cda5a29e551b1de0816b0de474c33 = $this->env->getExtension("native_profiler");
        $__internal_435eedc1e619370ae35625a4e6550244d26cda5a29e551b1de0816b0de474c33->enter($__internal_435eedc1e619370ae35625a4e6550244d26cda5a29e551b1de0816b0de474c33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 23
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/jquery.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/foundation.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/angular.min.js"), "html", null, true);
        echo "\" ></script>
            <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/angular-route.js"), "html", null, true);
        echo "\" ></script>
            <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/app.angular.js"), "html", null, true);
        echo "\" ></script>
            <script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_435eedc1e619370ae35625a4e6550244d26cda5a29e551b1de0816b0de474c33->leave($__internal_435eedc1e619370ae35625a4e6550244d26cda5a29e551b1de0816b0de474c33_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 28,  133 => 27,  129 => 26,  125 => 25,  121 => 24,  116 => 23,  110 => 22,  99 => 18,  88 => 14,  76 => 5,  67 => 30,  65 => 22,  60 => 19,  58 => 18,  53 => 15,  51 => 14,  44 => 11,  40 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         <link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}" />*/
/*         <link rel="stylesheet" href="{{ asset('css/main.css') }}" />*/
/* {#*/
/*         <link rel="stylesheet" href="{{ asset('css/main.scss') }}" />*/
/* #}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body ng-app="MyApp">*/
/*     {% block topbar %}{% endblock %}*/
/*     <div class="container">*/
/*         <div class="row">*/
/*             <div class="large-12 columns">*/
/*                 {% block body %}{% endblock %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*         {% block javascripts %}*/
/*             <script src="{{ asset('js/vendor/jquery.js') }}"></script>*/
/*             <script src="{{ asset('js/vendor/foundation.min.js') }}"></script>*/
/*             <script src="{{ asset('js/vendor/angular.min.js') }}" ></script>*/
/*             <script src="{{ asset('js/vendor/angular-route.js') }}" ></script>*/
/*             <script src="{{ asset('js/vendor/app.angular.js') }}" ></script>*/
/*             <script src="{{ asset('js/app.js') }}"></script>*/
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
