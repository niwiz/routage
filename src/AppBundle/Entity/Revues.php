<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Revues
 *
 * @ORM\Table(name="revues")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RevuesRepository")
 */
class Revues
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_revue", type="string", length=255)
     */
    private $nomRevue;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_publication_annuelle", type="integer")
     */
    private $nbPublicationAnnuelle;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_publication", type="integer")
     */
    private $nbPublication;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=255)
     */
    private $img;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomRevue
     *
     * @param string $nomRevue
     *
     * @return Revues
     */
    public function setNomRevue($nomRevue)
    {
        $this->nomRevue = $nomRevue;

        return $this;
    }

    /**
     * Get nomRevue
     *
     * @return string
     */
    public function getNomRevue()
    {
        return $this->nomRevue;
    }

    /**
     * Set nbPublicationAnnuelle
     *
     * @param integer $nbPublicationAnnuelle
     *
     * @return Revues
     */
    public function setNbPublicationAnnuelle($nbPublicationAnnuelle)
    {
        $this->nbPublicationAnnuelle = $nbPublicationAnnuelle;

        return $this;
    }

    /**
     * Get nbPublicationAnnuelle
     *
     * @return int
     */
    public function getNbPublicationAnnuelle()
    {
        return $this->nbPublicationAnnuelle;
    }

    /**
     * Set nbPublication
     *
     * @param integer $nbPublication
     *
     * @return Revues
     */
    public function setNbPublication($nbPublication)
    {
        $this->nbPublication = $nbPublication;

        return $this;
    }

    /**
     * Get nbPublication
     *
     * @return int
     */
    public function getNbPublication()
    {
        return $this->nbPublication;
    }

    /**
     * Set img
     *
     * @param string $img
     *
     * @return Revues
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }
}

