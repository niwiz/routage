<?php

/* AppBundle:head:topbar.html.twig */
class __TwigTemplate_a712db57e450ae92710266dc40c601fae8ce5b8740aff23830381beab7ee7909 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b8b5c737957f4889543bfc25e4da27b34aca334b4811ee2377ff7941d845513 = $this->env->getExtension("native_profiler");
        $__internal_2b8b5c737957f4889543bfc25e4da27b34aca334b4811ee2377ff7941d845513->enter($__internal_2b8b5c737957f4889543bfc25e4da27b34aca334b4811ee2377ff7941d845513_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:head:topbar.html.twig"));

        // line 1
        echo "<div class=\"title-bar\" data-responsive-toggle=\"main-menu\" data-hide-for=\"medium\">
    <button class=\"menu-icon\" type=\"button\" data-toggle></button>
    <div class=\"title-bar-title\">Menu</div>
</div>

<div class=\"top-bar\" id=\"main-menu\">
    <div class=\"top-bar-left\">
        <ul class=\"dropdown menu\" data-dropdown-menu>
            <li class=\"menu-text\">Site Title</li>
        </ul>
    </div>
    <div class=\"top-bar-right\">
        <ul class=\"menu\" data-responsive-menu=\"drilldown medium-dropdown\">
            <li class=\"has-submenu\">
                <a href=\"#\">One</a>
                <ul class=\"submenu menu vertical\" data-submenu>
                    <li><a href=\"#\">One</a></li>
                    <li><a href=\"#\">Two</a></li>
                    <li><a href=\"#\">Three</a></li>
                </ul>
            </li>
            <li><a href=\"#\">Two</a></li>
            <li><a href=\"#\">Three</a></li>
        </ul>
    </div>
</div>";
        
        $__internal_2b8b5c737957f4889543bfc25e4da27b34aca334b4811ee2377ff7941d845513->leave($__internal_2b8b5c737957f4889543bfc25e4da27b34aca334b4811ee2377ff7941d845513_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:head:topbar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium">*/
/*     <button class="menu-icon" type="button" data-toggle></button>*/
/*     <div class="title-bar-title">Menu</div>*/
/* </div>*/
/* */
/* <div class="top-bar" id="main-menu">*/
/*     <div class="top-bar-left">*/
/*         <ul class="dropdown menu" data-dropdown-menu>*/
/*             <li class="menu-text">Site Title</li>*/
/*         </ul>*/
/*     </div>*/
/*     <div class="top-bar-right">*/
/*         <ul class="menu" data-responsive-menu="drilldown medium-dropdown">*/
/*             <li class="has-submenu">*/
/*                 <a href="#">One</a>*/
/*                 <ul class="submenu menu vertical" data-submenu>*/
/*                     <li><a href="#">One</a></li>*/
/*                     <li><a href="#">Two</a></li>*/
/*                     <li><a href="#">Three</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*             <li><a href="#">Two</a></li>*/
/*             <li><a href="#">Three</a></li>*/
/*         </ul>*/
/*     </div>*/
/* </div>*/
