<?php

/* AppBundle:templates/container:table_routage.html.twig */
class __TwigTemplate_fb370b9b67c6a12bc6abef47b1b3bf3a6646875e4785da34a25afb9bec8f7700 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b0807e443b5a40aaffc92a7ca046d6ad82287cd1d3971ca0de61e86b2b9cd9ab = $this->env->getExtension("native_profiler");
        $__internal_b0807e443b5a40aaffc92a7ca046d6ad82287cd1d3971ca0de61e86b2b9cd9ab->enter($__internal_b0807e443b5a40aaffc92a7ca046d6ad82287cd1d3971ca0de61e86b2b9cd9ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:templates/container:table_routage.html.twig"));

        // line 1
        echo "<div class=\"large-9 column\">

    <div class=\"row\" style=\"\">
        <div class=\"imgRevues pull-right\">
            <img id=\"BTP_img\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/BTP_Gris.png"), "html", null, true);
        echo "\">
            <img id=\"Mat_img\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/Mat_Gris.png"), "html", null, true);
        echo "\">
            <img id=\"Rail_img\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/Rail_Gris.png"), "html", null, true);
        echo "\">
            <img id=\"Reseau_img\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/Reseau_Gris.png"), "html", null, true);
        echo "\">
            <img id=\"TC_img\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/TC_Gris.png"), "html", null, true);
        echo "\">
            <img id=\"B_img\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/B_Gris.png"), "html", null, true);
        echo "\">
        </div>
    </div>

    <div class=\"large-8 column\">
        <table class=\"hover\">
            <thead>
                <tr>
                    <th width=\"50\"></th>
                    <th width=\"150\">Code</th>
                    <th width=\"350\">Nom du fichier</th>
                    <th width=\"150\">Source</th>
                    <th width=\"150\">Date import</th>
                    <th width=\"150\">Qte</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fichiers"]) ? $context["fichiers"] : $this->getContext($context, "fichiers")));
        foreach ($context['_seq'] as $context["_key"] => $context["fichier"]) {
            // line 28
            echo "
            <tr data-id=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["fichier"], "id", array()), "html", null, true);
            echo "\">
                <td></td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["fichier"], "nomFichier", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["fichier"], "nomFamille", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["fichier"], "source", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["fichier"], "dateImp", array()), "d-m-Y"), "html", null, true);
            echo "</td>
                <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["fichier"], "qte", array()), "html", null, true);
            echo "</td>
            </tr>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fichier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "
            </tbody>
        </table>
    </div>

    <div class=\"large-4 column\">
        <div ng-view></div>

        ";
        // line 67
        echo "    </div>

</div>";
        
        $__internal_b0807e443b5a40aaffc92a7ca046d6ad82287cd1d3971ca0de61e86b2b9cd9ab->leave($__internal_b0807e443b5a40aaffc92a7ca046d6ad82287cd1d3971ca0de61e86b2b9cd9ab_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:templates/container:table_routage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 67,  106 => 39,  96 => 35,  92 => 34,  88 => 33,  84 => 32,  80 => 31,  75 => 29,  72 => 28,  68 => 27,  48 => 10,  44 => 9,  40 => 8,  36 => 7,  32 => 6,  28 => 5,  22 => 1,);
    }
}
/* <div class="large-9 column">*/
/* */
/*     <div class="row" style="">*/
/*         <div class="imgRevues pull-right">*/
/*             <img id="BTP_img" src="{{ asset('img/revues/BTP_Gris.png') }}">*/
/*             <img id="Mat_img" src="{{ asset('img/revues/Mat_Gris.png') }}">*/
/*             <img id="Rail_img" src="{{ asset('img/revues/Rail_Gris.png') }}">*/
/*             <img id="Reseau_img" src="{{ asset('img/revues/Reseau_Gris.png') }}">*/
/*             <img id="TC_img" src="{{ asset('img/revues/TC_Gris.png') }}">*/
/*             <img id="B_img" src="{{ asset('img/revues/B_Gris.png') }}">*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="large-8 column">*/
/*         <table class="hover">*/
/*             <thead>*/
/*                 <tr>*/
/*                     <th width="50"></th>*/
/*                     <th width="150">Code</th>*/
/*                     <th width="350">Nom du fichier</th>*/
/*                     <th width="150">Source</th>*/
/*                     <th width="150">Date import</th>*/
/*                     <th width="150">Qte</th>*/
/*                 </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for fichier in fichiers %}*/
/* */
/*             <tr data-id="{{ fichier.id }}">*/
/*                 <td></td>*/
/*                 <td>{{ fichier.nomFichier }}</td>*/
/*                 <td>{{ fichier.nomFamille }}</td>*/
/*                 <td>{{ fichier.source }}</td>*/
/*                 <td>{{ fichier.dateImp |date('d-m-Y') }}</td>*/
/*                 <td>{{ fichier.qte }}</td>*/
/*             </tr>*/
/* */
/*             {% endfor %}*/
/* */
/*             </tbody>*/
/*         </table>*/
/*     </div>*/
/* */
/*     <div class="large-4 column">*/
/*         <div ng-view></div>*/
/* */
/*         {#        <table>*/
/*                     <thead>*/
/* */
/*                     <tr>*/
/*                         <th width="50">291</th>*/
/*                         <th width="150">292</th>*/
/*                         <th width="350">+</th>*/
/*                     </tr>*/
/* */
/*                     </thead>*/
/*                     <tbody>*/
/* */
/*                     <tr>*/
/*                         <td></td>*/
/*                         <td></td>*/
/*                         <td></td>*/
/*                     </tr>*/
/* */
/*                     </tbody>*/
/*                 </table>#}*/
/*     </div>*/
/* */
/* </div>*/
