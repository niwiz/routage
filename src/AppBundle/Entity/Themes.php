<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Themes
 *
 * @ORM\Table(name="themes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ThemesRepository")
 */
class Themes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_theme", type="string", length=255)
     */
    private $nomTheme;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomTheme
     *
     * @param string $nomTheme
     *
     * @return Themes
     */
    public function setNomTheme($nomTheme)
    {
        $this->nomTheme = $nomTheme;

        return $this;
    }

    /**
     * Get nomTheme
     *
     * @return string
     */
    public function getNomTheme()
    {
        return $this->nomTheme;
    }
}

