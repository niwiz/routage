<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fichier
 *
 * @ORM\Table(name="fichier")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FichierRepository")
 */
class Fichier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_fichier", type="string", length=255)
     */
    private $nomFichier;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_famille", type="string", length=255)
     */
    private $nomFamille;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_imp", type="datetime", nullable=true)
     */
    private $dateImp;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer", nullable=true)
     */
    private $qte;

    /**
     * @var bool
     *
     * @ORM\Column(name="actif", type="boolean", nullable=true)
     */
    private $actif;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Themes")
     */
    protected $themes;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Numeros", mappedBy="fichier")
     */
    private $numeros;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomFichier
     *
     * @param string $nomFichier
     *
     * @return Fichier
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;

        return $this;
    }

    /**
     * Get nomFichier
     *
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * Set nomFamille
     *
     * @param string $nomFamille
     *
     * @return Fichier
     */
    public function setNomFamille($nomFamille)
    {
        $this->nomFamille = $nomFamille;

        return $this;
    }

    /**
     * Get nomFamille
     *
     * @return string
     */
    public function getNomFamille()
    {
        return $this->nomFamille;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return Fichier
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Fichier
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set dateImp
     *
     * @param \DateTime $dateImp
     *
     * @return Fichier
     */
    public function setDateImp($dateImp)
    {
        $this->dateImp = $dateImp;

        return $this;
    }

    /**
     * Get dateImp
     *
     * @return \DateTime
     */
    public function getDateImp()
    {
        return $this->dateImp;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     *
     * @return Fichier
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return int
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     *
     * @return Fichier
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return bool
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set themes
     *
     * @param \AppBundle\Entity\Themes $themes
     *
     * @return Fichier
     */
    public function setThemes(\AppBundle\Entity\Themes $themes = null)
    {
        $this->themes = $themes;

        return $this;
    }

    /**
     * Get themes
     *
     * @return \AppBundle\Entity\Themes
     */
    public function getThemes()
    {
        return $this->themes;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->numeros = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add numero
     *
     * @param \AppBundle\Entity\Numeros $numero
     *
     * @return Fichier
     */
    public function addNumero(\AppBundle\Entity\Numeros $numero)
    {
        $this->numeros[] = $numero;

        return $this;
    }

    /**
     * Remove numero
     *
     * @param \AppBundle\Entity\Numeros $numero
     */
    public function removeNumero(\AppBundle\Entity\Numeros $numero)
    {
        $this->numeros->removeElement($numero);
    }

    /**
     * Get numeros
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNumeros()
    {
        return $this->numeros;
    }
}
