<?php

/* AppBundle:templates/container:table_routage.html.twig */
class __TwigTemplate_3830be5b4e0a963959454372ce3fe84564adebd7089a2fd7829ea707bd2a9df3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"large-9 column\">
    <div class=\"row\" style=\"\">

        <div class=\"imgRevues pull-right\">
            <img id=\"BTP_img\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/BTP.png"), "html", null, true);
        echo "\">
            <img id=\"Mat_img\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/BTP.png"), "html", null, true);
        echo "\">
            <img id=\"Rail_img\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/BTP.png"), "html", null, true);
        echo "\">
            <img id=\"Reseau_img\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/BTP.png"), "html", null, true);
        echo "\">
            <img id=\"TC_img\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/BTP.png"), "html", null, true);
        echo "\">
            <img id=\"B_img\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/revues/BTP.png"), "html", null, true);
        echo "\">
        </div>

    </div>
    <div class=\"large-8 column\">
        <table class=\"hover\">
            <thead>
                <tr>
                    <th width=\"50\"></th>
                    <th width=\"150\">Code</th>
                    <th width=\"350\">Nom du fichier</th>
                    <th width=\"150\">Source</th>
                    <th width=\"150\">Date import</th>
                    <th width=\"150\">Qte</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>x</td>
                <td>tp102</td>
                <td>tp102-a-vrd-art 42</td>
                <td>ginger</td>
                <td>25/05/2016</td>
                <td>150</td>
            </tr>

            </tbody>
        </table>
    </div>
    <div class=\"large-4 column\">
        <table>
            <thead>
            <tr>
                <th width=\"50\">291</th>
                <th width=\"150\">292</th>
                <th width=\"350\">+</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            </tbody>
        </table>


    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "AppBundle:templates/container:table_routage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 10,  41 => 9,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* <div class="large-9 column">*/
/*     <div class="row" style="">*/
/* */
/*         <div class="imgRevues pull-right">*/
/*             <img id="BTP_img" src="{{ asset('img/revues/BTP.png') }}">*/
/*             <img id="Mat_img" src="{{ asset('img/revues/BTP.png') }}">*/
/*             <img id="Rail_img" src="{{ asset('img/revues/BTP.png') }}">*/
/*             <img id="Reseau_img" src="{{ asset('img/revues/BTP.png') }}">*/
/*             <img id="TC_img" src="{{ asset('img/revues/BTP.png') }}">*/
/*             <img id="B_img" src="{{ asset('img/revues/BTP.png') }}">*/
/*         </div>*/
/* */
/*     </div>*/
/*     <div class="large-8 column">*/
/*         <table class="hover">*/
/*             <thead>*/
/*                 <tr>*/
/*                     <th width="50"></th>*/
/*                     <th width="150">Code</th>*/
/*                     <th width="350">Nom du fichier</th>*/
/*                     <th width="150">Source</th>*/
/*                     <th width="150">Date import</th>*/
/*                     <th width="150">Qte</th>*/
/*                 </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             <tr>*/
/*                 <td>x</td>*/
/*                 <td>tp102</td>*/
/*                 <td>tp102-a-vrd-art 42</td>*/
/*                 <td>ginger</td>*/
/*                 <td>25/05/2016</td>*/
/*                 <td>150</td>*/
/*             </tr>*/
/* */
/*             </tbody>*/
/*         </table>*/
/*     </div>*/
/*     <div class="large-4 column">*/
/*         <table>*/
/*             <thead>*/
/*             <tr>*/
/*                 <th width="50">291</th>*/
/*                 <th width="150">292</th>*/
/*                 <th width="350">+</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             <tr>*/
/*                 <td></td>*/
/*                 <td></td>*/
/*                 <td></td>*/
/*             </tr>*/
/* */
/*             </tbody>*/
/*         </table>*/
/* */
/* */
/*     </div>*/
/* </div>*/
