<?php

/* AppBundle:templates/head:topbar.html.twig */
class __TwigTemplate_a73a157671844f844726d5cc08a0eb1bb32a89f4047dfa130320c9f08841f62b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd2a2f0693f0642f7a77578a74643e15e2114792c7fbaa6c462acded8fe14d3e = $this->env->getExtension("native_profiler");
        $__internal_dd2a2f0693f0642f7a77578a74643e15e2114792c7fbaa6c462acded8fe14d3e->enter($__internal_dd2a2f0693f0642f7a77578a74643e15e2114792c7fbaa6c462acded8fe14d3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:templates/head:topbar.html.twig"));

        // line 1
        echo "<div class=\"title-bar\" data-responsive-toggle=\"main-menu\" data-hide-for=\"medium\">
    <button class=\"menu-icon\" type=\"button\" data-toggle></button>
    <div class=\"title-bar-title\">Menu</div>
</div>

<div class=\"top-bar\" id=\"main-menu\">
    <div class=\"top-bar-left\">

        <ul class=\"dropdown menu\" data-dropdown-menu>
            <li class=\"menu-text\">Routage</li>
        </ul>

    </div>
    <div class=\"top-bar-right\">
        <ul class=\"menu\" data-responsive-menu=\"drilldown medium-dropdown\">
            <li class=\"has-submenu\">
                <a href=\"#\">One</a>
                <ul class=\"submenu menu vertical\" data-submenu>
                    <li><a href=\"#\">One</a></li>
                    <li><a href=\"#\">Two</a></li>
                    <li><a href=\"#\">Three</a></li>
                </ul>
            </li>
            <li><a href=\"#\">Two</a></li>
            <li><a href=\"#\">Three</a></li>
        </ul>
    </div>
</div>";
        
        $__internal_dd2a2f0693f0642f7a77578a74643e15e2114792c7fbaa6c462acded8fe14d3e->leave($__internal_dd2a2f0693f0642f7a77578a74643e15e2114792c7fbaa6c462acded8fe14d3e_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:templates/head:topbar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium">*/
/*     <button class="menu-icon" type="button" data-toggle></button>*/
/*     <div class="title-bar-title">Menu</div>*/
/* </div>*/
/* */
/* <div class="top-bar" id="main-menu">*/
/*     <div class="top-bar-left">*/
/* */
/*         <ul class="dropdown menu" data-dropdown-menu>*/
/*             <li class="menu-text">Routage</li>*/
/*         </ul>*/
/* */
/*     </div>*/
/*     <div class="top-bar-right">*/
/*         <ul class="menu" data-responsive-menu="drilldown medium-dropdown">*/
/*             <li class="has-submenu">*/
/*                 <a href="#">One</a>*/
/*                 <ul class="submenu menu vertical" data-submenu>*/
/*                     <li><a href="#">One</a></li>*/
/*                     <li><a href="#">Two</a></li>*/
/*                     <li><a href="#">Three</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*             <li><a href="#">Two</a></li>*/
/*             <li><a href="#">Three</a></li>*/
/*         </ul>*/
/*     </div>*/
/* </div>*/
