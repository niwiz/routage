<?php

/* AppBundle:Default:index.html.twig */
class __TwigTemplate_78c9e2053b4be8a4188a26a8cc6c2849de4afffbb0916ceaaee405cb90f10b42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'topbar' => array($this, 'block_topbar'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_topbar($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        $this->loadTemplate("AppBundle:templates/head:topbar.html.twig", "AppBundle:Default:index.html.twig", 10)->display($context);
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "
    ";
        // line 15
        $this->loadTemplate("AppBundle:templates/container:table_routage.html.twig", "AppBundle:Default:index.html.twig", 15)->display($context);
        // line 16
        echo "
";
    }

    // line 19
    public function block_javascripts($context, array $blocks = array())
    {
        $this->displayParentBlock("javascripts", $context, $blocks);
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 19,  46 => 16,  44 => 15,  41 => 14,  38 => 13,  33 => 10,  30 => 9,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* */
/* */
/* {#{% block titlepage %}*/
/*     {% include "AppBundle:Title:title.html.twig" %}*/
/* {% endblock %}#}*/
/* {#{% block container %}*/
/* {% endblock %}#}*/
/* {% block topbar %}*/
/*     {% include "AppBundle:templates/head:topbar.html.twig" %}*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*     {% include "AppBundle:templates/container:table_routage.html.twig" %}*/
/* */
/* {% endblock %}*/
/* */
/* {% block javascripts %}{{ parent() }}{% endblock %}*/
/* */
/* */
