<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Numeros
 *
 * @ORM\Table(name="numeros")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NumerosRepository")
 */
class Numeros
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fichier")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fichier;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Revues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $revue;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Numeros
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set fichier
     *
     * @param \AppBundle\Entity\Fichier $fichier
     *
     * @return Numeros
     */
    public function setFichier(\AppBundle\Entity\Fichier $fichier)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * Get fichier
     *
     * @return \AppBundle\Entity\Fichier
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * Set revue
     *
     * @param \AppBundle\Entity\Revues $revue
     *
     * @return Numeros
     */
    public function setRevue(\AppBundle\Entity\Revues $revue)
    {
        $this->revue = $revue;

        return $this;
    }

    /**
     * Get revue
     *
     * @return \AppBundle\Entity\Revues
     */
    public function getRevue()
    {
        return $this->revue;
    }
}
