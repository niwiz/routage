<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_fd8e168a697c58ec6f296d8b142006a8c6874ba662f75609d92a08241696e996 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce054985780af29bfdeba4f386830462ec10225242ff852457cd9c6e9d4a0d04 = $this->env->getExtension("native_profiler");
        $__internal_ce054985780af29bfdeba4f386830462ec10225242ff852457cd9c6e9d4a0d04->enter($__internal_ce054985780af29bfdeba4f386830462ec10225242ff852457cd9c6e9d4a0d04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce054985780af29bfdeba4f386830462ec10225242ff852457cd9c6e9d4a0d04->leave($__internal_ce054985780af29bfdeba4f386830462ec10225242ff852457cd9c6e9d4a0d04_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_e250a85dc2f1689beb50cb749faa6f5cc9a80f405d9e29376ae7961289bd84e0 = $this->env->getExtension("native_profiler");
        $__internal_e250a85dc2f1689beb50cb749faa6f5cc9a80f405d9e29376ae7961289bd84e0->enter($__internal_e250a85dc2f1689beb50cb749faa6f5cc9a80f405d9e29376ae7961289bd84e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_e250a85dc2f1689beb50cb749faa6f5cc9a80f405d9e29376ae7961289bd84e0->leave($__internal_e250a85dc2f1689beb50cb749faa6f5cc9a80f405d9e29376ae7961289bd84e0_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_27e7d54d6108a63c00f2e9e851d414e82e767fa6041cf60ce5dca284ba7e7b96 = $this->env->getExtension("native_profiler");
        $__internal_27e7d54d6108a63c00f2e9e851d414e82e767fa6041cf60ce5dca284ba7e7b96->enter($__internal_27e7d54d6108a63c00f2e9e851d414e82e767fa6041cf60ce5dca284ba7e7b96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_27e7d54d6108a63c00f2e9e851d414e82e767fa6041cf60ce5dca284ba7e7b96->leave($__internal_27e7d54d6108a63c00f2e9e851d414e82e767fa6041cf60ce5dca284ba7e7b96_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_172b9afb0e4642b2a9fc6aaec9d1a4e38a6e8b0f4085c096fa5eff92ea442716 = $this->env->getExtension("native_profiler");
        $__internal_172b9afb0e4642b2a9fc6aaec9d1a4e38a6e8b0f4085c096fa5eff92ea442716->enter($__internal_172b9afb0e4642b2a9fc6aaec9d1a4e38a6e8b0f4085c096fa5eff92ea442716_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_172b9afb0e4642b2a9fc6aaec9d1a4e38a6e8b0f4085c096fa5eff92ea442716->leave($__internal_172b9afb0e4642b2a9fc6aaec9d1a4e38a6e8b0f4085c096fa5eff92ea442716_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
