<?php

/* base.html.twig */
class __TwigTemplate_a692e4548487c282e54bf5ac64adcc2d6b7f40cb67ed14b569017dc2ea26f6c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca713b37b0b483e9ad42f6b033547ed388ff4159e92799555fa25087a76952f5 = $this->env->getExtension("native_profiler");
        $__internal_ca713b37b0b483e9ad42f6b033547ed388ff4159e92799555fa25087a76952f5->enter($__internal_ca713b37b0b483e9ad42f6b033547ed388ff4159e92799555fa25087a76952f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />

    </head>
    <body>
base



        ";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 18
        echo "
        ";
        // line 19
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "
    </body>
</html>
";
        
        $__internal_ca713b37b0b483e9ad42f6b033547ed388ff4159e92799555fa25087a76952f5->leave($__internal_ca713b37b0b483e9ad42f6b033547ed388ff4159e92799555fa25087a76952f5_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b4109310bbecc28fbab96272844d601da82b2964482b846bc437a8c2046e76a4 = $this->env->getExtension("native_profiler");
        $__internal_b4109310bbecc28fbab96272844d601da82b2964482b846bc437a8c2046e76a4->enter($__internal_b4109310bbecc28fbab96272844d601da82b2964482b846bc437a8c2046e76a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b4109310bbecc28fbab96272844d601da82b2964482b846bc437a8c2046e76a4->leave($__internal_b4109310bbecc28fbab96272844d601da82b2964482b846bc437a8c2046e76a4_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_c729436dd2e2fd9f15aa1a8ba1516637955903fd8c83cb89faaca8356bc1f103 = $this->env->getExtension("native_profiler");
        $__internal_c729436dd2e2fd9f15aa1a8ba1516637955903fd8c83cb89faaca8356bc1f103->enter($__internal_c729436dd2e2fd9f15aa1a8ba1516637955903fd8c83cb89faaca8356bc1f103_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/foundation.min.css"), "html", null, true);
        echo "\" />
        ";
        
        $__internal_c729436dd2e2fd9f15aa1a8ba1516637955903fd8c83cb89faaca8356bc1f103->leave($__internal_c729436dd2e2fd9f15aa1a8ba1516637955903fd8c83cb89faaca8356bc1f103_prof);

    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        $__internal_2184ac64ce20c437d449dc05a9d2e4fb9c9d85bdb04a89fa8104b9f544ac8188 = $this->env->getExtension("native_profiler");
        $__internal_2184ac64ce20c437d449dc05a9d2e4fb9c9d85bdb04a89fa8104b9f544ac8188->enter($__internal_2184ac64ce20c437d449dc05a9d2e4fb9c9d85bdb04a89fa8104b9f544ac8188_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_2184ac64ce20c437d449dc05a9d2e4fb9c9d85bdb04a89fa8104b9f544ac8188->leave($__internal_2184ac64ce20c437d449dc05a9d2e4fb9c9d85bdb04a89fa8104b9f544ac8188_prof);

    }

    // line 19
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_324a56af5d02ec8d1c790e9398bde827e6c69c63d77ff3b5727e3dbddab57b8f = $this->env->getExtension("native_profiler");
        $__internal_324a56af5d02ec8d1c790e9398bde827e6c69c63d77ff3b5727e3dbddab57b8f->enter($__internal_324a56af5d02ec8d1c790e9398bde827e6c69c63d77ff3b5727e3dbddab57b8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 20
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/vendor/foundation.min.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_324a56af5d02ec8d1c790e9398bde827e6c69c63d77ff3b5727e3dbddab57b8f->leave($__internal_324a56af5d02ec8d1c790e9398bde827e6c69c63d77ff3b5727e3dbddab57b8f_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 20,  106 => 19,  95 => 17,  85 => 7,  79 => 6,  67 => 5,  57 => 22,  55 => 19,  52 => 18,  50 => 17,  38 => 9,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*             <link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}" />*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/* */
/*     </head>*/
/*     <body>*/
/* base*/
/* */
/* */
/* */
/*         {% block body %}{% endblock %}*/
/* */
/*         {% block javascripts %}*/
/*             <script src="{{ asset('js/vendor/foundation.min.js') }}"></script>*/
/*         {% endblock %}*/
/* */
/*     </body>*/
/* </html>*/
/* */
