<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class NumerosController extends Controller
{
    /**
     * @Route("/revues/{name}", name="revues")
     */
    public function numerosAction($name)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Fichier');

        $fichiers =  $repository->findAll();
        //var_dump($fichiers);
        return ['fichiers' => $fichiers ];
        // replace this example code with whatever you need
        //return $this->render('index.html.twig');
    }

}
